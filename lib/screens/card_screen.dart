import 'package:flutter/material.dart';
import 'package:fl_components/widgets/widgets.dart';

class CardScreen extends StatelessWidget {
  const CardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Card widhet'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        children: const [
          CustomCardType1(),
          SizedBox(height: 10),
          CustomCardType2(
              name: 'siiuuum',
              imageUrl:
                  'https://scontent-mia3-1.xx.fbcdn.net/v/t1.6435-9/146147252_2829644243957515_1462967488530443044_n.jpg?stp=cp0_dst-jpg_e15_q65_s320x320&_nc_cat=108&ccb=1-5&_nc_sid=110474&_nc_ohc=wmPwYzM8sjEAX9LF7w1&_nc_ht=scontent-mia3-1.xx&oh=00_AT9Zv1Nc94DaiHVk-LIEi0yIq2qllfN9B1-pSG4V0hm0hg&oe=62538A3C'),
          SizedBox(height: 10),
          CustomCardType2(
              imageUrl:
                  'https://i.pinimg.com/564x/e0/68/0d/e0680db8a33ed52fa70cd2b6f83113ec.jpg')
        ],
      ),
    );
  }
}
