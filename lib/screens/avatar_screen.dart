import 'package:flutter/material.dart';

class AvatarSCreen extends StatelessWidget {
  const AvatarSCreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Avatars'),
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 5),
            child: CircleAvatar(
              backgroundColor: Colors.indigo[900],
              child: const Text('SL'),
            ),
          )
        ],
      ),
      body: const Center(
          child: CircleAvatar(
        maxRadius: 110,
        backgroundImage: NetworkImage(
            'https://scontent-mia3-2.xx.fbcdn.net/v/t1.6435-9/149726571_342168460490669_8873652721836500472_n.jpg?stp=cp0_dst-jpg_e15_p320x320_q65&_nc_cat=109&ccb=1-5&_nc_sid=85a577&_nc_ohc=QDm-RXPwSPsAX9a8F2K&_nc_ht=scontent-mia3-2.xx&oh=00_AT-MwLNJ2K74vDbeYe7Vv7xa7S5otrj6etX4gh7c25-_ow&oe=62531458'),
      )),
    );
  }
}
