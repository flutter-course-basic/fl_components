import 'package:fl_components/theme/app_theme.dart';
import 'package:flutter/material.dart';

class SliderScreen extends StatefulWidget {
  const SliderScreen({Key? key}) : super(key: key);

  @override
  State<SliderScreen> createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {
  double _sliderValue = 100;

  bool _sliderEnabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Slider and checks'),
      ),
      body: Column(
        children: [
          Slider.adaptive(
              activeColor: AppTheme.primary,
              thumbColor: Colors.red,
              min: 50,
              max: 400,
              value: _sliderValue,
              onChanged: !_sliderEnabled
                  ? null
                  : (value) {
                      setState(() {
                        _sliderValue = value;
                      });
                    }),
          CheckboxListTile(
              activeColor: AppTheme.primary,
              title: const Text('Enabled'),
              value: _sliderEnabled,
              onChanged: (value) {
                setState(() {
                  _sliderEnabled = value ?? true;
                });
              }),
          SwitchListTile.adaptive(
              activeColor: AppTheme.primary,
              title: const Text('Enabled'),
              value: _sliderEnabled,
              onChanged: (value) {
                setState(() {
                  _sliderEnabled = value;
                });
              }),
          const AboutListTile(),
          Expanded(
            child: SingleChildScrollView(
              child: Image(
                image: const NetworkImage(
                    'https://i.pinimg.com/564x/fa/12/67/fa126744a3d2dac4a31da69612fa4a27.jpg'),
                fit: BoxFit.contain,
                width: _sliderValue,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
