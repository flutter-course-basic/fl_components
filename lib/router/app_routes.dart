import 'package:flutter/material.dart';
import 'package:fl_components/screens/screens.dart';
import 'package:fl_components/models/models.dart';

class AppRoutes {
  static const String initialRoute = 'home';

  static final menuOptions = <MenuOption>[
    MenuOption(
        route: 'home',
        name: 'Home Screen',
        icon: Icons.home,
        screen: const HomeScreen()),
    MenuOption(
        route: 'listview1',
        name: 'List view 1',
        icon: Icons.list,
        screen: const ListView1Screen()),
    MenuOption(
        route: 'listview2',
        name: 'List view 2',
        icon: Icons.list,
        screen: const ListView2Screen()),
    MenuOption(
        route: 'card',
        name: 'Card',
        icon: Icons.card_giftcard,
        screen: const CardScreen()),
    MenuOption(
        route: 'alert',
        name: 'Alert',
        icon: Icons.add_alert_sharp,
        screen: const AlertScreen()),
    MenuOption(
        route: 'avatar',
        name: 'Circle Avatar',
        icon: Icons.supervised_user_circle_outlined,
        screen: const AvatarSCreen()),
    MenuOption(
        route: 'animated',
        name: 'Animated Container',
        icon: Icons.play_circle_outline_rounded,
        screen: const AnimatedScreen()),
    MenuOption(
        route: 'inputs',
        name: 'Text inputs',
        icon: Icons.input_rounded,
        screen: const InputsScreen()),
    MenuOption(
        route: 'slider',
        name: 'Slider and checks',
        icon: Icons.signal_wifi_connected_no_internet_4,
        screen: const SliderScreen()),
    MenuOption(
        route: 'listviewbuilder',
        name: 'InfiniteScroll & Pull to refresh',
        icon: Icons.build_circle_outlined,
        screen: const ListViewBuilderScreen()),
  ];

  static Map<String, Widget Function(BuildContext)> getMenuAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};

    appRoutes.addAll({
      'home': (BuildContext context) => const HomeScreen(),
    });

    for (final option in menuOptions) {
      appRoutes.addAll({
        option.route: (context) => option.screen,
      });
    }

    return appRoutes;
  }

  // static Map<String, Widget Function(BuildContext)> routes = {
  //   'home': (BuildContext context) => const HomeScreen(),
  //   'listview1': (BuildContext context) => const ListView1Screen(),
  //   'listview2': (BuildContext context) => const ListView2Screen(),
  //   'card': (BuildContext context) => const CardScreen(),
  //   'alert': (BuildContext context) => const AlertScreen(),
  // };

  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => const AlertScreen(),
    );
  }
}
